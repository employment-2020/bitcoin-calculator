import ReactDOM from 'react-dom';
import { renderToDOM } from '..';

jest.mock('react-dom');

it('renders without crashing', () => {
  const div = document.createElement('div');

  renderToDOM(div);

  expect(ReactDOM.render).toHaveBeenCalledWith(expect.anything(), div);
});
