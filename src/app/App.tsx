import React, { FC } from 'react';
import { Provider } from 'react-redux';
import BitcoinCalculatorView from 'views/BitcoinCalculatorView';
import store from './store';

const App: FC = () => (
  <Provider store={store}>
    <BitcoinCalculatorView />
  </Provider>
);

export default App;
