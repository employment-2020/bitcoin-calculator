import { render } from '@testing-library/react';
import React from 'react';
import App from '../App';

test('renders bitcoin calculator header', () => {
  const { getByText } = render(<App />);
  const headerElement = getByText(/Bitcoin Calculator/i);
  expect(headerElement).toBeInTheDocument();
});
