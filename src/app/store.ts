import { getDefaultMiddleware } from '@reduxjs/toolkit'
import { createStore } from 'redux-dynamic-modules'

const store = createStore(
  {},
  {
    id: 'store',
    middlewares: getDefaultMiddleware(),
  }
)

export default store
