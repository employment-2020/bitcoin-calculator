import React from 'react'
import { DynamicModuleLoader, IModuleTuple } from 'redux-dynamic-modules'

const withModules = <P extends object>(
  Component: React.ComponentType<P>,
  modules: IModuleTuple
) => (props: P) => (
  <DynamicModuleLoader modules={modules}>
    <Component {...(props as P)} />
  </DynamicModuleLoader>
)

export default withModules
