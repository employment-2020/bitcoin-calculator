import App from 'app/App';
import React from 'react';
import ReactDOM from 'react-dom';

export const renderToDOM = (container: HTMLElement | null) => {
  ReactDOM.render(
    <React.StrictMode>
      <App />
    </React.StrictMode>,
    container
  );
};

renderToDOM(document.getElementById('root'));
