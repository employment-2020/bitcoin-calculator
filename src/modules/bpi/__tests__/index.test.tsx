import AxiosMockAdapter from 'axios-mock-adapter';
import {
  api,
  BpiCurrentPrice,
  bpiReducer,
  BpiState,
  fetchCurrentPrice,
  selectBpiCurrentPrice,
  selectBpiHasError,
  selectBpiIsLoading,
  sliceName,
} from '..';

const mockApi = new AxiosMockAdapter(api);

const bitcoinCurrentPrice: BpiCurrentPrice = {
  time: {
    updated: 'Jun 11, 2020 14:58:00 UTC',
    updatedIso: '2020-06-11T14:58:00+00:00',
    updateduk: 'Jun 11, 2020 at 15:58 BST',
  },
  disclaimer:
    'This data was produced from the CoinDesk Bitcoin Price Index (USD). Non-USD currency data converted using hourly conversion rate from openexchangerates.org',
  chartName: 'Bitcoin',
  bpi: {
    usd: {
      code: 'USD',
      symbol: '&#36;',
      rate: '9,609.7982',
      description: 'United States Dollar',
      rateFloat: 9609.7982,
    },
    gbp: {
      code: 'GBP',
      symbol: '&pound;',
      rate: '7,578.5367',
      description: 'British Pound Sterling',
      rateFloat: 7578.5367,
    },
    eur: {
      code: 'EUR',
      symbol: '&euro;',
      rate: '8,456.9588',
      description: 'Euro',
      rateFloat: 8456.9588,
    },
  },
};

const fetchFailedErrorMessage = 'Could not fetch current price';

describe('bpi actions', () => {
  describe('fetchCurrentPrice', () => {
    it('fulfilled', async () => {
      mockApi
        .onGet('bpi/currentprice.json')
        .replyOnce(200, bitcoinCurrentPrice);

      const dispatch = jest.fn();
      const action = await fetchCurrentPrice()(dispatch, () => {}, undefined);

      expect(dispatch.mock.calls).toEqual([
        [fetchCurrentPrice.pending(action.meta.requestId)],
        [
          fetchCurrentPrice.fulfilled(
            bitcoinCurrentPrice,
            action.meta.requestId
          ),
        ],
      ]);
    });

    it('rejected', async () => {
      mockApi.onGet('bpi/currentprice.json').networkErrorOnce();

      const dispatch = jest.fn();
      const action = await fetchCurrentPrice()(dispatch, () => {}, undefined);

      expect(dispatch.mock.calls).toEqual([
        [fetchCurrentPrice.pending(action.meta.requestId)],
        [fetchCurrentPrice.rejected(action.error, action.meta.requestId)],
      ]);
    });
  });
});

describe('bpiReducer', () => {
  describe('fetchCurrentPrice', () => {
    it('pending', () => {
      const state: BpiState = {
        [sliceName]: bpiReducer(
          {
            currentPrice: bitcoinCurrentPrice,
            isLoading: false,
            error: {
              message: fetchFailedErrorMessage,
            },
          },
          fetchCurrentPrice.pending('')
        ),
      };

      expect(selectBpiCurrentPrice(state)).toEqual(bitcoinCurrentPrice);
      expect(selectBpiIsLoading(state)).toBe(true);
      expect(selectBpiHasError(state)).toBe(true);
    });

    it('fulfilled', () => {
      const state: BpiState = {
        [sliceName]: bpiReducer(
          {
            currentPrice: null,
            isLoading: true,
            error: {
              message: fetchFailedErrorMessage,
            },
          },
          fetchCurrentPrice.fulfilled(bitcoinCurrentPrice, '')
        ),
      };

      expect(selectBpiCurrentPrice(state)).toEqual(bitcoinCurrentPrice);
      expect(selectBpiIsLoading(state)).toBe(false);
      expect(selectBpiHasError(state)).toBe(false);
    });

    it('rejected', () => {
      const state: BpiState = {
        [sliceName]: bpiReducer(
          {
            currentPrice: bitcoinCurrentPrice,
            isLoading: true,
            error: null,
          },
          fetchCurrentPrice.rejected(new Error(fetchFailedErrorMessage), '')
        ),
      };

      expect(selectBpiCurrentPrice(state)).toEqual(bitcoinCurrentPrice);
      expect(selectBpiIsLoading(state)).toBe(false);
      expect(selectBpiHasError(state)).toBe(true);
    });
  });
});
