import {
  createAsyncThunk,
  createSelector,
  createSlice,
  Dictionary,
  SerializedError,
} from '@reduxjs/toolkit';
import axios from 'axios';
import applyConverters from 'axios-case-converter';
import { IModule } from 'redux-dynamic-modules';

export const sliceName = 'bpi';

export const api = applyConverters(
  axios.create({
    baseURL: process.env.REACT_APP_COINDESK_API_URL,
  })
);

export interface Bpi {
  code: string;
  symbol: string;
  rate: string;
  description: string;
  rateFloat: number;
}

export interface BpiCurrentPrice {
  time: {
    updated: string;
    updatedIso: string;
    updateduk: string;
  };
  disclaimer: string;
  chartName: string;
  bpi: Dictionary<Bpi>;
}

export const fetchCurrentPrice = createAsyncThunk(
  `${sliceName}/fetchCurrentPrice`,
  async () => (await api.get<BpiCurrentPrice>('bpi/currentprice.json')).data
);

const slice = createSlice({
  name: sliceName,
  initialState: {
    currentPrice: null as BpiCurrentPrice | null,
    isLoading: false,
    error: null as SerializedError | null,
  },
  reducers: {},
  extraReducers: (builder) =>
    builder
      .addCase(fetchCurrentPrice.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(fetchCurrentPrice.fulfilled, (state, action) => {
        state.currentPrice = action.payload;
        state.isLoading = false;
        state.error = null;
      })
      .addCase(fetchCurrentPrice.rejected, (state, action) => {
        state.isLoading = false;
        state.error = action.error;
      }),
});

export interface BpiState {
  [sliceName]: ReturnType<typeof slice.reducer>;
}

export const bpiReducer = slice.reducer;

const selectState = (state: BpiState) =>
  state[sliceName] as ReturnType<typeof slice.reducer>;

export const selectBpiCurrentPrice = createSelector(
  [selectState],
  (state) => state.currentPrice
);

export const selectBpiIsLoading = createSelector(
  [selectState],
  (state) => state.isLoading
);

export const selectBpiHasError = createSelector(
  [selectState],
  (state) => !!state.error
);

const bpiModule: IModule<BpiState> = {
  id: sliceName,
  reducerMap: {
    [sliceName]: slice.reducer,
  },
};

export default bpiModule;
