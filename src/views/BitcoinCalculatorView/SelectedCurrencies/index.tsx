import { BpiCurrentPrice } from 'modules/bpi';
import React, { ChangeEvent, FC, useCallback } from 'react';

interface Props {
  btc: string;
  currentPrice: BpiCurrentPrice | null;
  value: string[];
  onChange: (value: string[]) => void;
}

const SelectedCurrencies: FC<Props> = ({
  btc,
  currentPrice,
  value,
  onChange,
}) => {
  const onSelectChange = useCallback(
    (event: ChangeEvent<HTMLSelectElement>) => {
      onChange([...value, event.target.value]);
    },
    [value, onChange]
  );

  const onCurrencyRemove = useCallback(
    (code: string) => {
      const newSelectedCurrencies = [...value];
      const index = newSelectedCurrencies.indexOf(code.toLowerCase());

      if (index !== -1) {
        newSelectedCurrencies.splice(index, 1);
        onChange(newSelectedCurrencies);
      }
    },
    [value, onChange]
  );

  return (
    <div>
      {value.map((code) => {
        const bpi = currentPrice && currentPrice.bpi[code.toLowerCase()];

        if (!bpi) return null;

        return (
          <p key={code}>
            <span dangerouslySetInnerHTML={{ __html: bpi.symbol }} />
            <span>
              {(bpi.rateFloat * parseFloat(btc) || 0)
                .toFixed(2)
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}{' '}
            </span>
            <button onClick={() => onCurrencyRemove(bpi.code)}>X</button>
          </p>
        );
      })}
      {currentPrice && value.length < Object.keys(currentPrice.bpi).length && (
        <select onChange={onSelectChange} value="">
          <option>-- Convert to --</option>
          {currentPrice &&
            Object.values(currentPrice.bpi)
              .filter((bpi) => bpi && !value.includes(bpi.code.toLowerCase()))
              .map((bpi) => {
                if (!bpi) return null;

                const value = bpi.code.toLowerCase();

                return (
                  <option key={value} value={value}>
                    {bpi.code}
                  </option>
                );
              })}
        </select>
      )}
    </div>
  );
};

SelectedCurrencies.defaultProps = {
  value: [],
};

export default SelectedCurrencies;
