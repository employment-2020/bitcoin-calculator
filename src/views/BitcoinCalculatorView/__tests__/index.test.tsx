import store from 'app/store';
import React from 'react';
import { Provider } from 'react-redux';
import renderer from 'react-test-renderer';
import BitcoinCalculatorView from '..';

it('renders correctly', () => {
  expect(
    renderer
      .create(
        <Provider store={store}>
          <BitcoinCalculatorView />
        </Provider>
      )
      .toJSON()
  ).toMatchSnapshot();
});
