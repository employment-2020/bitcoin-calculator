import withModules from 'components/withModules';
import bpiModule, {
  fetchCurrentPrice,
  selectBpiCurrentPrice,
} from 'modules/bpi';
import React, { FC, useEffect } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import SelectedCurrencies from './SelectedCurrencies';

interface BitcoinCalculatorForm {
  btc: string;
  selectedCurrencies: string[];
}

const BitcoinCalculatorView: FC = () => {
  const dispatch = useDispatch();
  const currentPrice = useSelector(selectBpiCurrentPrice);
  const { register, watch, control } = useForm<BitcoinCalculatorForm>();
  const { btc } = watch();

  useEffect(() => {
    dispatch(fetchCurrentPrice());
    const interval = setInterval(() => dispatch(fetchCurrentPrice()), 60000);

    return () => clearInterval(interval);
  }, [dispatch]);

  return (
    <div>
      <h1>Bitcoin Calculator</h1>
      <p>
        <input name="btc" type="number" ref={register} />
        <label> BTC</label>
      </p>
      <Controller
        control={control}
        name="selectedCurrencies"
        render={(props) => (
          <SelectedCurrencies
            btc={btc}
            currentPrice={currentPrice}
            {...props}
          />
        )}
      />
    </div>
  );
};

export default withModules(BitcoinCalculatorView, [bpiModule]);
